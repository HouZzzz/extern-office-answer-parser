package parser;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        while (true) {
            generateFile();
        }
    }
    public static void generateFile() throws IOException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        List<String> questions = new ArrayList<>();

        // получаем ссылку на тест
        System.out.print("Введи ссылку на тест: ");
        String url = scanner.nextLine();

        // кол-во вопросов в тесте
        int questionsCount = 1;

        // название теста
        String testTitle = "";

        long startTime = System.nanoTime();
        questions.add(url + "\n\n");

        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("action", "login");
        mapParams.put("login", "login");
        mapParams.put("pwl", "password");

        // вход в систему
        Connection.Response res = Jsoup
                .connect("https://extern-office.net/student/")
                .data(mapParams)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.124 YaBrowser/22.9.2.1500 Yowser/2.5 Safari/537.36")
                .method(Connection.Method.POST)
                .execute();
        System.out.println("\nвход в систему прошел успешно");

        // куки для сохранения аккаунта в системе
        Map<String, String> cookies = res.cookies();

        // достаем одну страницу из теста для получения нужной информации
        Document firstTestPage = Jsoup
                .connect(url)
                .cookies(cookies)
                .get();

        // узнаем сколько всего вопросов
        String getQuestionsCount = firstTestPage
                .getElementsByTag("h3")
                .first()
                .toString()
                .replaceAll("</h3>", "");

        questionsCount = Integer.parseInt(getQuestionsCount.split(" ")[getQuestionsCount.split(" ").length - 1]);

        // узнаем название теста
        testTitle = firstTestPage
                .getElementsByTag("h2")
                .get(2)
                .toString()
                .replaceAll("</h2>", "")
                .split(">")[1]
                .replaceAll("\\.","");

        System.out.println("название теста: " + testTitle);
        System.out.println("общее количество вопросов: " + questionsCount + "\n");

        for (int i = 1; i <= questionsCount; i++) {
            System.out.println(i + " вопрос сканируется");
            Document doc = Jsoup
                    .connect(url + "?q=" + i)
                    .cookies(cookies)
                    .get();

            // находим встроенную в html ссылку на ответ
            Matcher matcher = Pattern.compile("https://extern-office.net\\?question=s.+'").matcher(doc.toString());

            String answerUrl = "";

            while (matcher.find()) {
                answerUrl = matcher.group().substring(0, matcher.group().length() - 1);
            }


            // находим ответ на вопрос
            Elements answerParts = Jsoup.connect(answerUrl)
                    .get()
                    .getElementsByTag("p");

            String question = doc.getElementById("question_body").child(0).html()
                    .replaceAll("<p>", "")
                    .replaceAll("</p>", "");
//            System.out.println(question);

            String answer = "";
            for (Element part : answerParts) {
                answer += part.toString();
            }
            answer = answer.replaceAll("&nbsp;", "")
                    .replaceAll("<p>", "")
                    .replaceAll("</p>", "")
                    .replaceAll("<br>", "\n");
//            System.out.println(answerUrl);

//            System.out.println(question + "\n");
//            System.out.println(answer);
//            System.out.println("\n\n\n");

            // добавляем результаты в список для дальнейшей записи в txt файл
            questions.add("--------------------------------------------------");
            questions.add(i + " вопрос:\n" + question + "\n");
            questions.add("Ответ:\n" + answer);
            questions.add("--------------------------------------------------");
            questions.add("\n\n");
//            Thread.sleep(300);
        }

        System.out.println("\n--------------------------------------------------");
        System.out.println("ВОПРОСЫ СКОПИРОВАНЫ");
        System.out.println("--------------------------------------------------");


        // записываем все найденое в файл
        Path file = Paths.get(String.format("%s.txt", testTitle));
        Files.write(file, questions, StandardCharsets.UTF_8);

        System.out.printf("ФАЙЛ %s СОЗДАН\n", testTitle);

        long endTime = System.nanoTime();
        long ms = ((endTime - startTime) / 1000000);
        System.out.println("НА ВЫПОЛНЕНИЕ УШЛО " + (ms / 1000) + " СЕКУНД\n");
    }
}
